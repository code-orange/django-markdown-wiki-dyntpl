from django.urls import path

from . import views

urlpatterns = [
    path("<str:content_package>/<str:lang_code>/<str:content_file>", views.wiki_page),
]
