from django_mdat_customer.django_mdat_customer.models import *


class WikiDynTplKeywords(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.CASCADE)
    name = models.CharField(max_length=250)
    value = models.CharField(max_length=250)

    class Meta:
        db_table = "wiki_dyntpl_keywords"
        unique_together = (("customer", "name"),)
