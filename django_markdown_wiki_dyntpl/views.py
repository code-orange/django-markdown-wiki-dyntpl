import os

from django.conf import settings
from django.http import HttpResponse, HttpResponseNotFound
from django.template import loader


def wiki_page(request, content_package, lang_code, content_file):
    file_found = False
    content_file_path = os.path.join(
        settings.WIKI_CONTENT_DIR, content_package, "content", lang_code, content_file
    )

    if not file_found and os.path.isfile(content_file_path + ".md"):
        file_found = True
        content_file_path += ".md"

    if not file_found and os.path.isfile(content_file_path + ".rst"):
        file_found = True
        content_file_path += ".rst"

    if not file_found:
        return HttpResponseNotFound()

    template = loader.get_template("django_markdown_wiki_main/markdown_content.html")

    template_opts = dict()
    template_opts["content"] = open(content_file_path).read()

    return HttpResponse(template.render(template_opts, request))
